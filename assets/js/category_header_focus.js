function focusElement() {

  var SN = SpatialNavigation;
    // Initialize
    SN.init();
    SN.add({
      id: 'upperbox',
      selector: '#upperbox .focusable',
      //restrict: 'self-only',
      // Force to focus the "#button-settings" when entering this section.
      defaultElement: '#upperbox .active',
      //enterTo: 'default-element'
    });
    SN.add({
      id: 'middlebox',
      selector: '#middlebox .focusable',
      //restrict: 'self-only',
      // Focus the last focused element first then entering this section.
      //enterTo: 'last-focused'
      defaultElement: '#middlebox .active',
    });   
    SN.add({
      id: 'down-menu',
      selector: '#down-menu .focusable',
      restrict: 'self-only',
      // Focus the last focused element first then entering this section.
      //enterTo: 'last-focused'
    });       
    
    $('.main-menu .focusable, .head-right .focusable').on('sn:focused', function() {     
      $(this).css({'transform': 'scale(1.1)'})
    });
   
    $('.main-menu .focusable, .head-right .focusable').on('sn:unfocused', function() {     
      $(this).css({'transform': 'scale(1)'})
    });
    $(document).on("sn:focused", ".no-shadow.focusable", function(ev) {          
      $(this).parents(".owl-item").prev(".owl-item").find(".focusable").focus();
    });
    /*$('#middlebox .focusable').on('sn:willfocus', function() {    
      SN.pause();
      $('.child-menu').hide();
      $(this).ensureVisible(function() {
        SN.focus(this);
        SN.resume();
      });
      return false;
    });*/



   /* $('.focusable')
      .on('sn:enter-down', function() {
        // Add "clicking" style.
        $(this).addClass('active');

      })
      .on('sn:enter-up', function() {
        var id = this.id;
        var $this = $(this);
        // Remove "clicking" style.
        $this.removeClass('active');
        
        // For testing only.
      });*/
          
    
    SN.makeFocusable();
    // Focus section "upperbox" by default.
    SN.focus('upperbox');
 

}

