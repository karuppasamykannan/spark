 function focusElement() {   
  var SN = SpatialNavigation;
  // Initialize
  SN.init();
  SN.add({
    id: 'moviesectionbox',
    selector: '#moviesectionbox .focusable',
    //restrict: 'self-only',
    
    defaultElement: '#moviesectionbox #show_play_url', 
       
  });   

  $('.focusable')
  .on('sn:enter-down', function() {
    // Add "clicking" style.
    $(this).addClass('active');

  })
  .on('sn:enter-up', function() {
    var id = this.id;
    var $this = $(this);
    // Remove "clicking" style.
    $this.removeClass('active');
    
    // For testing only.
    console.log(id);
  });

  SN.add({
    id: 'pop-dialog',
    selector: '#pop-dialog .focusable',
    // Since it's a standalone dialog, we restrict its navigation to
    // itself so the focus won't be moved to another section.
    restrict: 'self-only',
    // Note that we don't set "enterTo" to "default-element" in this
    // section because it's impossible to enter this section from the
    // others by arrow keys. This default element will only affect the
    // "focus('settings-dialog')" API call.
    defaultElement: '#exit_app_ok'
  });

  
    // Focus section "moviesectionbox" by default.
  SN.focus('moviesectionbox');
  

  //console.log("SN element function called")
}   