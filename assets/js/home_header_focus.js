function focusElement() {
  var SN = SpatialNavigation;
    // Initialize
    SN.init();
    SN.add({
      id: 'upperbox',
      selector: '#upperbox .focusable',
      //restrict: 'self-only',
      // Force to focus the "#button-settings" when entering this section.
      defaultElement: '#upperbox .active',
      //enterTo: 'default-element'
    });
    SN.add({
      id: 'middlebox',
      selector: '#middlebox .focusable',
      //restrict: 'self-only',
      // Focus the last focused element first then entering this section.
      //enterTo: 'last-focused'
    });   
   SN.add({
    id: 'pop-container',
    selector: '#pop-container .focusable',
    enterTo: '#exit_app_ok'

   })
    $('.main-menu .focusable, .head-right .focusable').on('sn:focused', function() {     
      $(this).css({'transform': 'scale(1.1)'})
    });
    
    $('.main-menu .focusable, .head-right .focusable').on('sn:unfocused', function() {     
      $(this).css({'transform': 'scale(1)'})
    });
    $('.main-menu .active').css({'transform': 'scale(1.1)'}) 
    $(document).on("sn:focused", ".no-shadow.focusable", function(ev) {          
      $(this).parents(".owl-item").prev(".owl-item").find(".focusable").focus();
    });
    /*if (window.outerWidth > 1399) {
      $(document).on("sn:focused", ".element_6 .focusable", function(ev) {                
        $(this).parents(".owl-stage").addClass("leftpos_change")
      });
    }
    else {
      $(document).on("sn:focused", ".element_5 .focusable", function(ev) {                
        $(this).parents(".owl-stage").addClass("leftpos_change")        
      });  
      
    }  
    $(document).on("keydown", ".leftpos_change .element_0 .focusable", function(inEvent) {                    
      if(inEvent.keyCode == 37) {
        $(this).parents(".leftpos_change").css("left", "50px");
      }
      else {
        $(this).parents(".leftpos_change").css("left", "-50px");  
      }
    });*/
    /*$('#middlebox .focusable').on('sn:willfocus', function() {
      SN.pause();
      $('.child-menu').hide();
      $(this).ensureVisible(function() {
        SN.focus(this);
        SN.resume();
      });
      return false;
    });*/
    $('.focusable')
      .on('sn:enter-down', function() {
        // Add "clicking" style.
        $(this).addClass('active');

      })
      .on('sn:enter-up', function() {
        var id = this.id;
        var $this = $(this);
        // Remove "clicking" style.
        $this.removeClass('active');
        
        // For testing only.
      });
      
      $('#exit_app_ok.focusable')
      .on('sn:enter-down', function() {
        $(".pop-container").hide();
        $('#pop-container').addClass('hide');
        //localStorage.removeItem("shm_webos_app_launch")
        //window.close();
      });

      $('#exit_app_cancel.focusable')
      .on('sn:enter-down', function() {
        $(".pop-container").hide();
        $('#pop-container').addClass('hide');
         SN.focus('middlebox');
      });


     SN.add({
      id: 'pop-dialog',
      selector: '#pop-dialog .focusable',
      // Since it's a standalone dialog, we restrict its navigation to
      // itself so the focus won't be moved to another section.
      restrict: 'self-only',
      // Note that we don't set "enterTo" to "default-element" in this
      // section because it's impossible to enter this section from the
      // others by arrow keys. This default element will only affect the
      // "focus('settings-dialog')" API call.
      defaultElement: '#exit_app_ok'
    });
      
    $('#upperbox #last_item').keydown( function(inEvent){
      console.log("code:"+ inEvent.keyCode);
      console.log("event:"+ inEvent);
        /*var message = "<h1>" + "Keycode is <lg_red>" + inEvent.keyCode + "</lg_red></h1>"+ "</br>";
        document.getElementById("sdfsaf").innerHTML = message;
        console.log(message);
        if(inEvent.keyCode == 39) {
          console.log("abc");
          (".see-more").removeClass('focusable')
        }
        else {
          console.log("def");
          (".see-more").addClass('focusable')                  
        }*/
    });


    $("#logout").on('sn:enter-down',function(){
      $('#pop-container').removeClass('hide');
      $(".pop-container").show();
       SN.focus('pop-dialog');
       
    });

    if (window.history && window.history.pushState) {
      console.log("window.history"+window.history);
      console.log("window.history.pushState"+window.history.pushState);
      window.history.pushState(null, null, './');
      $(window).on('popstate', function() {
        window.history.pushState(null, null, './');
        $('#pop-container').removeClass('hide');
        $(".pop-container").show();
         SN.focus('pop-dialog');
         
      });        
    }

    SN.makeFocusable();
    // Focus section "middlebox" by default.
    SN.focus('upperbox');
 

}

