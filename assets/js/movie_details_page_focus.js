function focusElement() {
  var SN = SpatialNavigation;
  // Initialize
  SN.init();
  SN.add({
    id: 'moviesectionbox',
    selector: '#moviesectionbox .focusable',
    defaultElement: '#moviesectionbox #watchTrailer', 
  });   


  $('.focusable')
    .on('sn:enter-down', function() {
      // Add "clicking" style.
      $(this).addClass('active');

    })
    .on('sn:enter-up', function() {
      var id = this.id;
      var $this = $(this);
      // Remove "clicking" style.
      $this.removeClass('active');
      
      // For testing only.
      console.log(id);
    });

   
   SN.add({
    id: 'pop-dialog1',
    selector: '#pop-dialog1 .focusable',
    // Since it's a standalone dialog, we restrict its navigation to
    // itself so the focus won't be moved to another section.
    restrict: 'self-only',
    // Note that we don't set "enterTo" to "default-element" in this
    // section because it's impossible to enter this section from the
    // others by arrow keys. This default element will only affect the
    // "focus('settings-dialog')" API call.
    defaultElement: '#login_ok'
  });    

  $('#exit_app_ok.focusable')
        .on('sn:enter-down', function() {
          $(".pop-container").hide();
          $('#pop-container').addClass('hide');
          //localStorage.removeItem("shm_webos_app_launch")
          //window.close();
        });
  
        $('#exit_app_cancel.focusable')
        .on('sn:enter-down', function() {
          $(".pop-container").hide();
          $('#pop-container').addClass('hide');
           SN.focus('middlebox');
        });
  
  
       SN.add({
        id: 'pop-dialog',
        selector: '#pop-dialog .focusable',
        // Since it's a standalone dialog, we restrict its navigation to
        // itself so the focus won't be moved to another section.
        restrict: 'self-only',
        // Note that we don't set "enterTo" to "default-element" in this
        // section because it's impossible to enter this section from the
        // others by arrow keys. This default element will only affect the
        // "focus('settings-dialog')" API call.
        defaultElement: '#exit_app_ok'
      }); 

      $(document).on("sn:focused", ".no-shadow.focusable", function(ev) {          
        $(this).parents(".owl-item").prev(".owl-item").find(".focusable").focus();
      });
       
  SN.makeFocusable();
    // Focus section "moviesectionbox" by default.
  SN.focus('moviesectionbox');
}   