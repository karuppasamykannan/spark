function focusElement() {

 var SN = SpatialNavigation;
  // Initialize
  SN.init();
  SN.add({
    id: 'upperbox',
    selector: '#upperbox .focusable',
    //restrict: 'self-only',
    // Force to focus the "#button-settings" when entering this section.
    defaultElement: '#upperbox .active',
    //enterTo: 'default-element'
  });
  SN.add({
    id: 'middlebox',
    selector: '#middlebox .focusable',
    //restrict: 'self-only',
    // Focus the last focused element first then entering this section.
    enterTo: 'last-focused'
  });   
  SN.add({
    id: 'down-menu',
    selector: '#down-menu .focusable',
    restrict: 'self-only',
    // Focus the last focused element first then entering this section.
    //enterTo: 'last-focused'
  });   
  $('.main-menu .focusable, .head-right .focusable').on('sn:focused', function() {     
    $(this).css({'transform': 'scale(1.1)'})
  });
 
  $('.main-menu .focusable, .head-right .focusable').on('sn:unfocused', function() {     
    $(this).css({'transform': 'scale(1)'})
  });

  $('#middlebox .list_items_bottom .focusable').on('sn:willfocus', function() {        
    $(".load-more-wrap").show();
  });


  $('body').on('sn:willfocus', '#middlebox .list_items_bottom .focusable', function() {    
    $(".load-more-wrap").show();
  });  

  $('#middlebox .focusable').on('sn:willfocus', function() {
    //SN.remove('last_item')
    SN.pause();
    $('.child-menu').hide();
    $(this).ensureVisible(function() {
      SN.focus(this);
      SN.resume();
    });
    return false;
  });

  $('#middlebox #list_title.focusable').on('sn:willfocus', function() {            
    SN.pause();
    $("#upperbox .focusable").focus();   
    $(this).ensureVisible(function() {
      SN.focus("#upperbox .focusable");
      SN.resume();
    });
    return false;
  });

 
  SN.makeFocusable();
  // Focus section "middlebox" by default.
  SN.focus('upperbox'); 

}

