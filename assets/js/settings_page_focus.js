function focusElement() {

  var SN = SpatialNavigation;
    // Initialize
    SN.init();
    SN.add({
      id: 'upperbox',
      selector: '#upperbox .focusable',
      //restrict: 'self-only',
      // Force to focus the "#button-settings" when entering this section.
      defaultElement: '#upperbox .active',
      //enterTo: 'default-element'
    });
    SN.add({
      id: 'middlebox',
      selector: '#middlebox .focusable',
      //restrict: 'self-only',
      defaultElement: '#s_first',
      // Focus the last focused element first then entering this section.
      enterTo: '#s_first'
    });   
   SN.add({
    id: 'pop-container',
    selector: '#pop-container .focusable',
    enterTo: '#exit_app_ok'

   })

    $('.main-menu .focusable, .head-right .focusable').on('sn:focused', function() {     
      $(this).css({'transform': 'scale(1.1)'})
    });
    
    $('.main-menu .focusable, .head-right .focusable').on('sn:unfocused', function() {     
      $(this).css({'transform': 'scale(1)'})
    });
    $('.main-menu .active').css({'transform': 'scale(1.1)'}) 
    
    $('#exit_app_ok.focusable')
      .on('sn:enter-down', function() {
        $(".pop-container").hide();
        $('#pop-container').addClass('hide');
        //localStorage.removeItem("shm_webos_app_launch")
        //window.close();
      });

      $('#exit_app_cancel.focusable')
      .on('sn:enter-down', function() {
        $(".pop-container").hide();
        $('#pop-container').addClass('hide');
         SN.focus('middlebox');
      });


     SN.add({
      id: 'pop-dialog',
      selector: '#pop-dialog .focusable',
      // Since it's a standalone dialog, we restrict its navigation to
      // itself so the focus won't be moved to another section.
      restrict: 'self-only',
      // Note that we don't set "enterTo" to "default-element" in this
      // section because it's impossible to enter this section from the
      // others by arrow keys. This default element will only affect the
      // "focus('settings-dialog')" API call.
      defaultElement: '#exit_app_ok'
    });
    
     SN.add({
      id: 'pop-dialog1',
      selector: '#pop-dialog1 .focusable',
      restrict: 'self-only',
      defaultElement: '#retry'
    });
      
    $('#upperbox #last_item').keydown( function(inEvent){
      console.log("code:"+ inEvent.keyCode);
      console.log("event:"+ inEvent);
    });


    $("#logout").on('sn:enter-down',function(){
      $('#pop-container').removeClass('hide');
      $("#pop-container").show();
       SN.focus('pop-dialog');
       
    });
    

    SN.makeFocusable();
    // Focus section "middlebox" by default.
    SN.focus('middlebox');
  }  